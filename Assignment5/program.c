#include <stdio.h>
#define pP 500
#define pA 3
int attendees(int price);
int revenue(int price);
int cost(int price);
int profit(int price);

int attendees(int price){
   return (120-(price-15)/5*20);
   }
int revenue(int price){
   return price*attendees(price);
   }
int cost(int price){
   return pP + attendees(price)*pA;
   }
int profit(int price){
   return revenue(price)-cost(price);
   }
int main(){
   int max = 0, value, att;
   printf("Profit Analysis (Rs.5 to Rs.45)\n\n");
   printf("Ticket Price\t Attendees \t Income \n");
	for(int tP = 5 ; tP<50; tP+=5){
      printf("Rs.%d \t\t %d \t\t Rs.%d\n", tP, attendees(tP), profit(tP));
      }
   for(int tP = 5; tP<=50; tP+=5)
    {  
		   if(max < profit(tP)){
		      max = profit(tP);
            value = tP;
            att = attendees(tP);
         }      
    }   
   printf("\nThe profit is high when the ticket price is Rs.%d\n", value);
   printf("The maximum proft is Rs.%d with %d attendees.\n", max, att);
    return 0;
}
