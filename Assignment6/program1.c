#include <stdio.h>
void pattern(int n);
void row(int r);
int rN = 1, num;
void pattern(int n){
   if(n>0){
      row(rN);
      printf("\n\n");
      rN++;
      pattern(n-1);
   }
}
void row(int r){
   if(r>0){
      printf("%d",r);
      row(r-1);
   }
}
int main(){
   printf("No.of Lines (Pattern No): ");
   scanf("%d", &num);
   pattern(num);
   return 0;
}
