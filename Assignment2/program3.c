#include <stdio.h>
int main()
{
    int a,b,temp;
    printf("Enter the first number: ");
    scanf("%d", &a);
    printf("Enter the second number: ");
    scanf("%d", &b);
    temp = a;
    a = b;
    b = temp;
    printf("First Number is: %d\n", a);
    printf("Second Number is: %d\n", b);
    return 0;
}
