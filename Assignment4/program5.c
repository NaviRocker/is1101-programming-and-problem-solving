#include <stdio.h>
int main (){
    int n, x;
    printf("Input a Number: ");
    scanf("%d", &n);
    printf("Maximum Number: ");
    scanf("%d", &x);
    for (int i=1; i<=n; i++){
        printf("Multiplication Table for %d\n", i);
        for (int c = 1; c<= x; c++){
            printf("%d * %d = %d\n", i, c, i*c);
            }
        printf("\n");
        }
    return 0;
}
