#include <stdio.h>
int main(){
    int no, last = 0, rev = 0;
    printf("Input a Number: ");
    scanf("%d", &no);
    while (no!=0){
    last = no % 10;
    rev = rev*10 + last;
    no = no/10;
    }
    printf("Reversed Number is %d", rev);
    return 0;
}
