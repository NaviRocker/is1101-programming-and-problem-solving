#include <stdio.h>
#include <ctype.h>
int main(){
  char c;
  printf("Input a Character: ");
  scanf("%c", &c);
  if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'A' || c =='E' || c=='I' || c == 'O' || c == 'U'){
    printf("%c is a Vowel", c);}
  else if(!isalpha(c)){
    printf("%c is not a Valid Character", c);}
  else{
    printf("%c is a Consonant", c);}
  return 0;
}
