#include <stdio.h>
int main(){
    int n;
    printf("Input a Number: ");
    scanf("%d", &n);
    (n > 0)?
    (printf("%d is a Positive Number", n)):
    ((n < 0)?
      (printf("%d is a Negative Number", n)):
      (printf("It is Zero"))
    );
    return 0;
}
