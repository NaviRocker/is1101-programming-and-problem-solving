#include <stdio.h>
int main(){
  char c;
  printf("Input a Character: ");
  scanf("%c", &c);
  (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'A' || c =='E' || c=='I' || c == 'O' || c == 'U')?
  (printf("%c is a Vowel", c)):
  (printf("%c is a Consonant", c));
  return 0;
}
